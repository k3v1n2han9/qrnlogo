package hello;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.oned.Code128Writer;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.krysalis.barcode4j.impl.code128.Code128Bean;
import org.krysalis.barcode4j.output.bitmap.BitmapCanvasProvider;
import org.krysalis.barcode4j.tools.UnitConv;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import net.glxn.qrgen.core.image.ImageType;
import org.springframework.web.bind.annotation.*;

import java.awt.image.BufferedImage;
import java.io.*;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;


import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

import org.springframework.http.*;

@RestController
public class Services {
    @RequestMapping("/")
    @ResponseBody
    public String abc() {
        return "<script>window.location='http://www.zimolo.com'</script>";
    }

    @RequestMapping(value = "/qr", method = RequestMethod.GET)
    public ResponseEntity<byte[]> getImage(@RequestParam(value="code") String code) throws IOException {
        if(code != "")
            QR(code);
        else
            return null;
        RandomAccessFile f = new RandomAccessFile
                ("code.jpg", "r");
        byte[] b = new byte[(int) f.length()];
        f.readFully(b);
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.IMAGE_PNG);
        return new ResponseEntity<byte[]>(b, headers, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/logo", method = RequestMethod.GET)
    public ResponseEntity<byte[]> getLogo(@RequestParam(value = "tenant") String tenant) throws IOException {
        RandomAccessFile f = new RandomAccessFile("logo/" + tenant + ".png", "r");
        byte[] b = new byte[(int) f.length()];
        f.readFully(b);
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.IMAGE_PNG);
        return new ResponseEntity<>(b, headers, HttpStatus.CREATED);
    }

    @RequestMapping("/alllogo")
    @ResponseBody
    public String allLogos(HttpServletRequest request) {
        File folder = new File("logo");
        File[] listOfFiles = folder.listFiles();
        Arrays.sort(listOfFiles);
        String output = "<style>div{float:left}</style>";
        String url = request.getRequestURL().toString().replace("alllogo", "logo");
        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile() && listOfFiles[i].getName().contains("png")) {
                output += "<div><table border='1'><tr><th>" + listOfFiles[i].getName().split("\\.")[0] +
                        "</th></td><tr><td><img src='" + url + "?tenant=" +
                        listOfFiles[i].getName().split("\\.")[0] +
                        "' height='100px' /></td></tr></table></div>";
            }
        }
        return output;
    }

    private void QR(String qr) {
        File file = net.glxn.qrgen.javase.QRCode.from(qr).to(ImageType.JPG).file();
        String filePath = "code.jpg";
        try {
            File dest = new File(filePath);
            Files.copy(file.toPath(), dest.toPath(), REPLACE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/bar", method = RequestMethod.GET)
    public ResponseEntity<byte[]> getBarcode(@RequestParam(value = "code") String code) throws Exception {
        try {
            int width = 400;
            int height = 100;
            String imageFormat = "png";
            BitMatrix bitMatrix = new Code128Writer().encode(code, BarcodeFormat.CODE_128, width, height);
            MatrixToImageWriter.writeToStream(bitMatrix, imageFormat, new FileOutputStream(new File("barcode.png")));
            RandomAccessFile f = new RandomAccessFile("barcode.png", "r");
            byte[] b = new byte[(int) f.length()];
            f.readFully(b);
            final HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.IMAGE_PNG);
            return new ResponseEntity<>(b, headers, HttpStatus.CREATED);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    @RequestMapping(value = "/bar2", method = RequestMethod.GET)
    public ResponseEntity<byte[]> getBarcode2(@RequestParam(value = "code") String code) throws Exception {
        float length = 3;
        int dpi = 150;
        Code128Bean bean = new Code128Bean();
        bean.setModuleWidth(UnitConv.in2mm(length / dpi));
        bean.doQuietZone(false);
        File outputFile = new File("4j.png");
        OutputStream out = new FileOutputStream(outputFile);
        try {
            BitmapCanvasProvider canvas = new BitmapCanvasProvider(
            out, "image/x-png", dpi, BufferedImage.TYPE_BYTE_BINARY, false, 0);
            bean.generateBarcode(canvas, code);
            canvas.finish();
        } catch(Exception e) {
            System.out.println(e.getMessage());
        } finally {
            out.close();
        }
        RandomAccessFile f = new RandomAccessFile("4j.png", "r");
        byte[] b = new byte[(int) f.length()];
        f.readFully(b);
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.IMAGE_PNG);
        return new ResponseEntity<>(b, headers, HttpStatus.CREATED);
    }
}

